//----------------------------------------------------------------------------//
//                                         888                                //
//                                         888                                //
//                                         888                                //
//                         8888b.  888d888 888  888                           //
//                            `88b 888P'   888 .88P                           //
//                        .d888888 888     888888K                            //
//                        888  888 888     888 `88b                           //
//                        `Y888888 888     888  888                           //
//                                                                            //
//                                                                            //
//  File      : canvas_utils.mjs                                              //
//  Project   : lissajous                                                     //
//  Date      : 15 Dec, 21                                                    //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2021                                                //
//                                                                            //
//  Description :                                                             //
//                                                                            //
//---------------------------------------------------------------------------~//

//----------------------------------------------------------------------------//
// Canvas_Utils                                                               //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
export class Canvas_Utils
{
    //--------------------------------------------------------------------------
    static create_with_size(width, height)
    {
        // Create the canvas that we gonna draw...
        const canvas = document.createElement("canvas");

        // @notice(stdmatt): [Attach property to the canvas object] - 15 Dec, 2021 at 08:26:45
        // Yes I know... but it's just easier to code as if it was a property...
        canvas.context_2d = canvas.getContext("2d");
        canvas.width      = width;
        canvas.height     = height;

        return canvas;
    }

    //------------------------------------------------------------------------//
    // Context Functions                                                      //
    //------------------------------------------------------------------------//
    //--------------------------------------------------------------------------
    static save(ctx)
    {
        ctx.save();

    }
    static restore(ctx)
    {
        ctx.restore();
    }

    static set_stroke(ctx, color, size)
    {
        Canvas_Utils.set_stroke_color(ctx, color);
        Canvas_Utils.set_stroke_size (ctx, size);
    }

    static set_stroke_color(ctx, color)
    {
        ctx.strokeStyle = color;
    }

    static set_stroke_size(ctx, size)
    {
        ctx.lineWidth = size;
    }

    static set_fill_color(ctx, color)
    {
        ctx.fillColor = color;
    }

    static rotate_by(ctx, angle)
    {
        ctx.rotate(angle);
    }

    static translate_by(ctx, x, y)
    {
        ctx.translate(x, y);
    }

    static clear(ctx)
    {
        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
    }

    static draw_line(ctx, x1, y1, x2, y2)
    {
        ctx.beginPath();
        ctx.moveTo(x1, y1);
        ctx.lineTo(x2, y2)
        ctx.closePath();
        ctx.stroke();
    }


}
