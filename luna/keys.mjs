export class Keys
{
    static A = 65;
    static B = 66;
    static C = 67;
    static D = 68;
    static E = 69;
    static F = 70;
    static G = 71;
    static H = 72;
    static I = 73;
    static J = 74;
    static K = 75;
    static L = 76;
    static M = 77;
    static N = 78;
    static O = 79;
    static P = 80;
    static Q = 81;
    static R = 82;
    static S = 83;
    static T = 84;
    static U = 85;
    static V = 86;
    static W = 87;
    static X = 88;
    static Y = 89;
    static Z = 90;

    static ARROW_LEFT  = 37;
    static ARROW_UP    = 38;
    static ARROW_RIGHT = 39;
    static ARROW_DOWN  = 40;

    static normalize_letters(code)
    {
        const key_code = (code - 32);
        if(key_code >= Keys.A && key_code <= Keys.Z) {
            return key_code;
        }

        return key_code;
    }
}