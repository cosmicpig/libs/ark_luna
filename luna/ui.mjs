import { Sprite } from "./sprite.mjs";

// @XXX(stdmatt): This is a first implementation so not caring too much
// of how the api is being developed, but there's a lot of complications
// arrising by the fact that we don't have the methods with enough
// clarity points.
// I mean, would be better to have a clear object tree and the functions
// operate upon the objects of that object tree - 8/2/2021, 3:34:43 AM
export class Fixed_Size_Container
    extends PIXI.Container
{
    //--------------------------------------------------------------------------
    constructor(width, height, color = 0xFF00FF)
    {
        super();

        this.bg       = Sprite.from_blank_texture(width, height);
        this.bg.alpha = 0.5; // @XXX(stdmatt): [DEBUG VALUE] - 15 Dec, 2021 at 03:34:41
        this.bg.tint  = color;

        this.addChild(this.bg);
    }

    //--------------------------------------------------------------------------
    _calculateBounds() // @pixi
    {
        // @notice(stdmatt): [Override Pixi _calculateBounds] - 15 Dec, 2021 at 03:34:59
        //   This makes the sprite have the size of the sprite that we added
        //   in the constructor and don't be affected by the other children...
        // @XXX(stdmatt): [Override Pixi _calculateBounds] - 15 Dec, 2021 at 03:38:02
        //   Check if there's another way to achieve the same result...
        //   This looks too hacky...
    } // _calculateBounds
}
