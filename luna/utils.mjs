export class Utils
{
    //
    // Unique Id
    //
    //--------------------------------------------------------------------------
    static generate_unique_id()
    {
        if(this.s_unique_id == undefined) {
            this.s_unique_id = 0;
        }
        return this.s_unique_id++;
    }

    //--------------------------------------------------------------------------
    static add_luna_unique_object_id(obj)
    {
        if(obj._luna_object_id === undefined) {
            obj._luna_object_id = Utils.generate_unique_id();
        }
    }

    //--------------------------------------------------------------------------
    static get_luna_unique_object_id(obj)
    {
        Utils.add_luna_unique_object_id(obj);
        return obj._luna_object_id;
    }

    //
    //
    //
    //--------------------------------------------------------------------------
    static get_url_params()
    {
        const params = new URLSearchParams(location.search);
        return params;
    }


    //
    // Color
    //
    //--------------------------------------------------------------------------
    static COLOR_WHITE = 0xFFFFFF;


    //
    // Path
    //
    //--------------------------------------------------------------------------
    static basename(path)
    {
        const slash_index = path.lastIndexOf("/");
        if(slash_index == -1) {
            return path;
        }
        const filename = path.substr(slash_index+1);
        return filename;
    }

    static find_extension(filename)
    {
        const dot_index = filename.lastIndexOf(".");
        if(dot_index == -1) {
            return "";
        }
        const ext = filename.substr(dot_index).toLowerCase();
        return ext;
    }

    static remove_extension(filename)
    {
        const dot_index = filename.lastIndexOf(".");
        if(dot_index == -1) {
            return "";
        }
        const clean_name = filename.substr(0, dot_index);
        return clean_name;
    }

    //
    //
    //
    static get_value_or_default(value, default_value)
    {
        // @TODO(stdmatt): Add NaN - 8/7/2021, 9:38:06 AM
        if(value === null || value === undefined) {
            return default_value;
        }

        return value;
    }
}
