//----------------------------------------------------------------------------//
//                                         888                                //
//                                         888                                //
//                                         888                                //
//                         8888b.  888d888 888  888                           //
//                            `88b 888P'   888 .88P                           //
//                        .d888888 888     888888K                            //
//                        888  888 888     888 `88b                           //
//                        `Y888888 888     888  888                           //
//                                                                            //
//                                                                            //
//  File      : luna.mjs                                                      //
//  Project   : lissajous                                                     //
//  Date      : 15 Dec, 21                                                    //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2021                                                //
//                                                                            //
//  Description :                                                             //
//                                                                            //
//---------------------------------------------------------------------------~//

//----------------------------------------------------------------------------//
// Imports                                                                    //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
import * as Algo         from "./algo.mjs";
import * as UI           from "./ui.mjs";
import { App           } from "./app.mjs";
import { Arr           } from "./arr.mjs"
import { Audio_Manager } from "./audio_manager.mjs"
import { Base_Scene    } from "./base_scene.mjs"
import { Base_TTFText  } from "./base_text.mjs";
import { Input_Manager } from "./input_manager.mjs"
import { Layout        } from "./layout.mjs";
import { RES           } from "./res.mjs"
import { Rnd           } from "./rnd.mjs";
import { Scale_Utils   } from "./scale_utils.mjs"
import { Scene_Manager } from "./scene_manager.mjs"
import { Str           } from "./str.mjs";
import { Tween         } from "./tween.mjs";
import { Utils         } from "./utils.mjs"
import { GUI           } from "./gui.mjs";
import { Vec2          } from "./math_utils.mjs";
import { Size          } from "./math_utils.mjs";
import { Rect          } from "./math_utils.mjs";
import { Math_Utils    } from "./math_utils.mjs";
import { Log_Utils     } from "./log_utils.mjs";
import { Sprite        } from "./sprite.mjs";
import { Container     } from "./container.mjs";
import { Canvas_Utils  } from "./canvas_utils.mjs";
import { Keys          } from "./keys.mjs";

//------------------------------------------------------------------------------
// @XXX(stdmatt): [Hack to make ark as namespace] - 13 Dec, 2021 at 07:09:50
// Must be another way in javascript to group te things....
export let luna = {};

luna.Algo          = Algo;
luna.App           = App;
luna.Arr           = Arr;
luna.Audio_Manager = Audio_Manager;
luna.Base_Scene    = Base_Scene;
luna.Base_TTFText  = Base_TTFText;
luna.Input_Manager = Input_Manager;
luna.Layout        = Layout;
luna.RES           = RES;
luna.Rnd           = Rnd;
luna.Scale_Utils   = Scale_Utils;
luna.Scene_Manager = Scene_Manager;
luna.Str           = Str;
luna.Tween         = Tween;
luna.UI            = UI;
luna.Utils         = Utils;
luna.GUI           = GUI;
luna.Vec2          = Vec2
luna.Size          = Size;
luna.Rect          = Rect;
luna.Math_Utils    = Math_Utils;
luna.Log_Utils     = Log_Utils;
luna.Sprite        = Sprite;
luna.Container     = Container;
luna.Canvas_Utils  = Canvas_Utils;
luna.Keys          = Keys;


//------------------------------------------------------------------------------
// @notice(stdmatt): [Functions that we want to make at luna namespace] - 14 Dec, 2021 at 02:24:50
// This functions we want to make available to use on the luna namespace.
// They are still defined on the sub-namespace, but exposed here as well.
// Math Utils
luna.HALF_PI   = Math_Utils.HALF_PI;
luna.PI        = Math_Utils.PI;
luna.TWO_PI    = Math_Utils.TWO_PI;
luna.THREE_PI  = Math_Utils.THREE_PI;
luna.FOUR_PI   = Math_Utils.FOUR_PI;

luna.make_size = Math_Utils.make_size;
luna.make_rect = Math_Utils.make_rect;
luna.make_vec2 = Math_Utils.make_vec2;

luna.to_int    = Math_Utils.to_int;
// Log Utils
luna.log_debug   = Log_Utils.log_debug;
luna.log_verbose = Log_Utils.log_verbose;
luna.log_error   = Log_Utils.log_error;
luna.log_warn    = Log_Utils.log_error;
luna.log_always  - Log_Utils.log_always;